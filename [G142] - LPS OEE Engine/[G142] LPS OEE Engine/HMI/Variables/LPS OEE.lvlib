﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Count" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Qty Counter</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Qty Counter</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/[G081] - LPS OEE.lvproj/My Computer/HMI/Variables/LPS OEE.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Qty Counter</Property>
	</Item>
	<Item Name="Dies Change" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Dies Change</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Dies Change</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Dies Change</Property>
	</Item>
	<Item Name="Dies Change DT" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Dies Change DT</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Dies Change DT</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Dies Change DT</Property>
	</Item>
	<Item Name="Dies DT" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Dies DT</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Dies DT</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Dies DT</Property>
	</Item>
	<Item Name="Machine DT" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Machine DT</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Machine DT</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Machine DT</Property>
	</Item>
	<Item Name="Material DT" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Material DT</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Material DT</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Material DT</Property>
	</Item>
	<Item Name="No Production" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.No Production</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.No Production</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.No Production</Property>
	</Item>
	<Item Name="OPC1" Type="IO Server">
		<Property Name="atrb" Type="Str">0!)!!!Q1!!!,!!!!"B!!!!A!!!"E!'5!91"E!')!91"O!'1!!Q!!!!!!!!!!!!!!"B!!!!M!!!"F!'Y!91"C!'Q!:1"%!'5!9A"V!'=!!A!!!!!'%!!!$1!!!'E!&lt;A"U!'5!=A"G!'%!9Q"F!&amp;1!?1"Q!'5!"B!!!!Q!!!""!(-!?1"O!'-!;!"S!']!&lt;A"P!(5!=Q!'%!!!"Q!!!'Q!&lt;Q"H!%9!;1"M!'5!"B!!!!!!!!!'%!!!"Q!!!'U!91"D!'A!;1"O!'5!"B!!!!E!!!"M!']!9Q"B!'Q!;!"P!(-!&gt;!!'%!!!#Q!!!'U!91"Y!%9!;1"M!'5!5Q"J!(I!:1!$!!9!!!!!!!!!R%!'%!!!"A!!!&amp;!!=A"P!'=!31"%!!91!!!E!!!!4A"B!(1!;1"P!'Y!91"M!#!!31"O!(-!&gt;!"S!(5!&lt;1"F!'Y!&gt;!"T!#Y!4A"*!%]!5!"$!&amp;-!:1"S!(9!:1"S!(-!,A"7!$5!"B!!!"%!!!"S!'5!9Q"P!'Y!&lt;A"F!'-!&gt;!"1!']!&lt;!"M!&amp;)!91"U!'5!!Q!!!!!!!!!!!&amp;Z!"B!!!")!!!"T!'5!=A"W!'5!=A"*!'Y!=Q"U!'%!&lt;A"D!'5!6!"Z!(!!:1!'%!!!#Q!!!%]!&gt;1"U!#!!&lt;Q"G!#!!5!"S!']!9Q!'%!!!#A!!!(5!=!"E!'%!&gt;!"F!&amp;)!91"U!'5!!Q!!!!!!!!!!1)^!"B!!!!U!!!"V!(-!:1"5!'%!:Q"H!'5!=A"5!'E!&lt;1"F!!)!!!!!</Property>
		<Property Name="className" Type="Str">OPC</Property>
	</Item>
	<Item Name="Plan Production" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Plan Production</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Plan Production</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Plan Production</Property>
	</Item>
	<Item Name="Production DT" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Production DT</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Production DT</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Production DT</Property>
	</Item>
	<Item Name="Quality DT" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\LPS Ethernet.Device1.Quality DT</Property>
		<Property Name="Network:URL" Type="Str">\\.\My Computer\LPS OEE\OPC1\Mitsubishi Ethernet.LPS Ethernet.Quality DT</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\LPS OEE.lvlib\OPC1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">LPS Ethernet.Device1.Quality DT</Property>
	</Item>
</Library>
